# Docker

### 一、docker镜像基本用法

1. 查找镜像
```bash
# docker search 镜像名
# docker search centos
```
2. 拉取镜像

```bash
# docker pull 镜像名
# docker pull centos本地保存（打包）镜像
```
3. 查看本地镜像

```bash
# docker image ls
```

4. 删除本地镜像(-f 强制删除)

```bash
# docker rmi -f nginx
```

5. 本地保存（打包）镜像（离线镜像包）

```bash
# docker save -o centos.tar.gz
```

6. 加载本地镜像

```bash
# docker load -i centos.tar.gz
```

### 二、 容器使用


1. 运行容器（centos镜像支持/bin/bash）

```bash
# docker run --name=hello -it centos /bin/bash
```
2. 退出容器：exit（关闭容器）
```bash
查看容器
# docker  ps		#查看运行的
# docker  ps -a 		#查看所有
```
3. 以守护进程方式启动容器：
```bash
# docker run --name=hello1 -td centos
# docker ps    #查看容器状态
```
4. 交互式登录容器：
```bash
# docker exrc -it hello1 /bin/bash
# exit	#退出
# docker ps  #查看
```

5. docker容器启动、停止、删除(-f 强制删除)

```bash
# docker stop hello1
# docker start hello1
# docker rm -f hello1
```

6.  运行nginx容器。映射80端口

```bash
# docker run --name=nginx -p 80 -itd centos 
```
7. 交互式进入nginx容器，查看ip地址（docker 选项  参数  容器名 解释器）

```bash
# docker exec -it nginx /bin/bash
# ip a
```
8. 安装nginx并访问容器服务（查看nginx容器映射端口号）

```bash
# docker ps
# curl 主机ip:映射端口
```

### 三、 dockerfile 

dockerfile构建镜像可以使用docker build 命令。docker build 命令 -f 可以指定具体的dockerfile文件

```bash
# cat > dockerfile <<EOF
FROM centos
MAINTAINER vk
RUN yum -y install nginx
Run yum -y install wget
COPY index.html /usr/share/nginx/html/
EXPOSE 80
ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
EOF
# echo "nginx in docker with dockerfile" > index.html

```

拆分dickerfile

```bash
1. FROM centos
基础镜像centos（镜像应存在可用，可以下载下来或已下载）

2. MAINTAINER vk
作者信息（联系方式等信息）

3. RUN
当前镜像创建过程中运行的命令
	3.1 shell模式
	RUN　<command>
	RUN yum -y install httpd
	3.2 exec模式
	RUN [ "/bin/bash", "-c", "echo hello" ]
	等同于：
	RUN	/bin/bash -c echo hello

4. EXPOSE 80
	仅仅声名需要暴漏80端口，帮助使用者了解需要映射端口
	EXPOSE <端口1><端口2>

5. CMD
	类似于RUN。
	CMD在docker run 时运行
	RUN在docker build 构建时运行

6. ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
执行具体命令，当镜像创建后，通过镜像启动容器后自动启动nginx服务，“-g daemon off”是参数

7. COPY
	复制命令，可以复制中更改文件的所有者所属组
	COPY <src>...<dest>
	COPY [ "<src>"..."<dest>" ]
	COPY [--chown=<user>:<group>] <src>...<dest>
	COPY [--chown=<user>:<group>] ["<src>...<dest>"]
	源文件或目录可以使用通配符
	目标路径不存在会自动创建

8. ADD 
	与COPY类似,相同需求下，建议使用COPY
	ADD <src>...<dest>
	ADD [ "<src>"..."<dest>" ]
	与COPY不同是：当源文件是tar压缩文件（gzip、bzip2、xz）会自动解压到目标路径。根据是否自动解压源文件决定是否选用ADD

9. VOLUME
	定义匿名数据卷。在启动容器时，忘记挂载数据卷，会自动挂载到匿名卷。
	作用：1.避免重要数据丢失，因重启丢失数据
		 2.避免容器不断变大
	格式：
		VOLUME ["<路径1>", "<路径2>"...]
		VOLUME <路径>
	在启动容器docker run的时候，可以通过-v参数修改挂载点
	VOLUME ["/data"]

10. WORKDIR
指定工作目录，用WORKDIR指定的工作目录，会在构建镜像的每一层中都存在。（目录必须存在，提前创建好）
docker build 构建镜像过程中的每一个RUN命令都是新建的一层，只有WORKDIR创建的目录才会一直存在。
	WORKDIR <工作目录路径>
	WORKDIR /path/to/workdir	（绝对路径）

11. ENV
	设置环境变量
	ENV <key> <value>
	ENV <key>=<value>...
	ENV NODE_VERSION 6.6.6
	RUN curl -SL0 "http://centos/v$NODE_VERSION.txt"
	RUN curl -SL0 "http://centos/v6.6.6.txt"

12. USER
	用于指定执行后续命令的用户和用户组，这边只是切换后续执行命令的用户、用户组（用户、用户组必须已存在）
	USER <用户>[:<用户组>]
	USER daemon
	USER nginx
	USER uid
	USER user:group
	USER uid:gid(user:gid)(uid:group)

13. ONBUILD
	用于延迟构建命令的执行。简单说，就是Dockerfile中用ONBUILD指定的命令，在本次构建镜像的过程中不会执行（假设镜像为test-build）,当新的dockerfile 使用了之前构建的镜像FROM test-build,这时执行新的dockerfile 构建时，会执行test-build的dockerfile里的ONBUILD指定的命令。
	ONBUILD <其他指令>

14. LABEL
	LABEL用来给镜像添加一些元数据（metadata），以键值对的形式
	LABEL <key>=<value> <key>=<value>....

15. HELTHCHECK
	用于指定某个程序或者指令来监控docker容器服务的运行状态。
	HELTHCHECK [选项] CMD <命令>：设置检查容器健康状况的命令
	HRLTHCHECK NONE：如果基础镜像有健康检查指令，使用这行可以屏蔽掉其健康检查指令
	HELTHCHECK [选项] CMD <命令>： 这边CMD后边跟随的命令使用，可以参考CMD用法

16. ARG
	构建参数，与ENV作用一致。不过作用域不一样。ARG设置的镜像仅仅对Dockerfile内有效，也就是说只有docker build 的过程中有效，构建好的镜像内不存在此环境变量。
	构建命令docker build 中使用 --build-arg <参数名>=<值> 来覆盖。
	ARG <参数名>[=<默认值>]
	
```

