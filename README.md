# Docker 安装部署

### 一、 yum安装docker-ce

 1. 配置主机名

    ```bash
    # hostnamectl set-hostname docker01
    ```

    

 2. 关闭防火墙

    ```bash
    # systemctl stop firewalld
    ```

    

 3. 关闭iptables（可选安装iptables）

    ```bash
    # yum -y install iptables-services
    # service iptables stop
    # iptables -F
    ```

    

 4. 关闭selinux（永久关闭）

    ```bash
    # sed -i 's/SELINUX=enforcing/SELINUX=disabled/'  /etc/selinux/config
    # reboot
    # getenforce 0
    ```

    

 5. 进行时间同步（安装ntpdate或chrony）

    ```bash
    # yum -y install ntp
    # ntpdate cn.pool.ntp.org
    # crontab -e
    * */1 * * * ntpdate cn.pool.btp.org
    ```

    

 6. 安装基础软件包

 7. 安装docker-ce

    ```bash
    配置阿里云yum源
    # curl -o /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    # yum makecache
    # yum -y install docker-ce
    ```

 8. 开启包转发功能和修改内核参数

    ```bash
    内核参数修改：br_netfilter模块：用于将桥接流量转发至iptables链，br_netfilter内核参数需要开启转发
    ```

    ```bash
    # modprobe br_netfilter
    # cat > /etc/sysctl.d/docker.conf  <<EOF
    net.bridge.bridge-nf-call-ip6tables = 1
    net.bridge.bridge-nf-call-iptables = 1
    net.ipv4.ip_forward = 1
    EOF
    # sysctl -p /etc/sysctl.d/docker.conf
    
    ```

 9. 创建daemon.json指定镜像仓库

    ```bash
    # vi /etc/docker/daemon.json
    {
      "registry-mirrors": [ "https://mirrors.tuna.tsinghua.edu.cn", "https://XXXXXXXXXXX.com" ]
    }
    ```
 10. 重新加载守护线程
```bash
# systemctl daemon-reload
# systemctl restart docker
```

### 二、 二进制安装docker-ce

---- 

# Docker compose安装部署
 1. github查找docker/compose
https://github.com/docker/compose
 2. 获取最新稳定版本。

Linux  
You can download Docker Compose binaries from the release page on this repository.  
 
Rename the relevant binary for your OS to docker-compose and copy it to $HOME/.docker/cli-plugins  

Or copy it into one of these folders for installing it system-wide:  

/usr/local/lib/docker/cli-plugins OR /usr/local/libexec/docker/cli-plugins  
/usr/lib/docker/cli-plugins OR /usr/libexec/docker/cli-plugins  
(might require to make the downloaded file executable with chmod +x)  

Quick Start  
Using Docker Compose is basically a three-step process:   

Define your app's environment with a Dockerfile so it can be reproduced anywhere.  
Define the services that make up your app in docker-compose.yml so they can be run together in an isolated environment.  
Lastly, run docker compose up and Compose will start and run your entire app.  
A Compose file looks like this:  
```yaml
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
  redis:
    image: redis
```
 3. Install docker-compose
```bash
# yum -y install wget
# wget https://github.com/docker/compose/releases/download/v2.9.0/docker-compose-linux-x86_64
# mv ./docker-compose-linux-x86_64 /usr/local/bin/docker-compose
# chmod 777 /usr/local/bin/docker-compose

#docker-compose -v
Docker Compose version v2.9.0
```

 4. Install successfuly.